"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.conf.urls import include
from django.contrib import admin

from django.contrib.auth.views import login
from django.contrib import auth
import biblioteca.views

from django.conf import settings
from django.conf.urls.static import static

from django.db.models import Q

from django.contrib.auth.views import password_change

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', biblioteca.views.HomeView.as_view(), name = "home"),
    #hace busqueda
    url(r'^busqueda/$', biblioteca.views.Busqueda.as_view(), name = "busqueda"),
    #listar cosas
    url(r'^libros/$', biblioteca.views.LibrosListView.as_view(), name = "libros"),
    url(r'^libros-en/(?P<pk>(\d{0,3}))/$', biblioteca.views.LibroInCasilleroListView.as_view(), name ="libroscas"),
    url(r'^casilleros/$', biblioteca.views.CasillerosListView.as_view(), name = "casilleros"),
    url(r'^mis-libros/$', biblioteca.views.MyBooksListView.as_view(), name="propios"),
    url(r'^usuarios/$', biblioteca.views.UsuariosListView.as_view(), name="usuarios"),
    url(r'^biblioteca/(?P<usuario>\w+)$', biblioteca.views.PersonBooksListView.as_view(), name="biblioteca-user"),
    #Cambiar libros
    url(r'^prestar/(?P<identificador>\d+)/$', biblioteca.views.prestar, name = "prestar"),
    url(r'^devolver/(?P<identificador>\d+)/$', biblioteca.views.devolver, name = "devolver"),
    url(r'^biblioteca/prestar/(?P<identificador>\d+)/(?P<usuario>\w+)/$', biblioteca.views.prestar_administrador, name = "biblioteca-prestar"),
    #crear cosas
    url(r'^nuevo-libro/$', biblioteca.views.LibroCreateView.as_view(), name="nuevoLibro"),
    url(r'^nuevo-casillero/$', biblioteca.views.crear_casillero, name = "casillero-nuevo"),
    url(r'^login/$', biblioteca.views.login_user, name = "login"),
    url(r'^logout/$', auth.views.logout, name = "logout"),
    url(r'^all/$', biblioteca.views.LibrosListView.as_view(), name = "allbooks"),

    url(r'^labels/', include('labels.urls')),
    url(r'^design/', biblioteca.views.DesignLabels, name = "design"),
    #Editar libros
     url(r'edit/(?P<identificador>\d+)/?$', biblioteca.views.LibroUpdateView.as_view(), name="editar" ),
    url(r'^cambiar-contraseña/$', password_change, {'template_name': 'password_change_form.html','post_change_redirect': '/'}, name="password-change"),
]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
