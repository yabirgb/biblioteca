"""
pdf_templates.py
================

Module to handle pdf templates for different printing layouts
Each template defines a

1. DocTemplate
2. PageTemplate
3. Flowables

"""

"""
    Some definitions:
        -x1 and y1 are cordenates for the frames, depend on row and column
        -"showBoundary" shows a black line in the border of the frame
        -styleN is a dictionary(you can add more elements) and apply the style
            to the elements marked with this style. More info can be obtained
            from the documentation.

"""
from reportlab.platypus import Paragraph, Frame, Image
from reportlab.lib.units import inch, mm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_CENTER

class LabelPDF():
    def __init__(self, tag, type="default"):
        self.showBoundary = 0 #show a black line in the border
        self.tag = tag
        self.type = type
        self._set_values(type)
        self.slogan = []


    def make_content(self):
        """
        This needs to be done for every label becuase frame.addFrame clears out the
        flowables.
        """

        styles = getSampleStyleSheet()
        #You can apply different styles
        styleN = {"default": ParagraphStyle('default',fontName='Times-Roman',fontSize=12, alignment="TA_CENTER")}

        self.slogan = []
        #self.slogan.append(Paragraph(self.tag.slogan1,styleN["default"]))
        #self.slogan.append(Paragraph(self.tag.slogan2,styleN["default"]))
        for i in self.tag.libros.all():
            self.slogan.append([ Paragraph(str(i.identificador),styleN["default"]), Paragraph( i.clasificacion.__str__()+ "-" + i.autor_clasificaion,styleN["default"] )  ])

        #self.contact =[]
        #self.contact.append(Paragraph(self.tag.contact1,styleN["default"]))
        #self.contact.append(Paragraph(self.tag.contact2,styleN["default"]))
        #self.contact.append(Paragraph(self.tag.contact3,styleN["default"]))

        #self.url=[Paragraph(self.tag.url,styleN["default"])]

        #self.qrcode=[]
        #if self.tag.qrcode:
        #    self.qrcode.append(Image(self.tag.qrcode.path, 1.0*inch, 1.0*inch))

        #self.logo=[]
        #if self.tag.logo:
        #    self.logo.append(Image(self.tag.logo.path, 0.4*self.height, 0.4*self.height))


    def draw_frames(self, canvas ):

        if self.type=='apli38':
            """
                I needed to print in a stickers paper an the layout was 3 columns 8 rows
                I just needed to text lines so I designed this one. I leave it hear
                because might be useful.
            """
            counter = 0
            pos = 1

            for j in range(0,self.nCols):
                for i in reversed(range(0, self.nRows)):
                    x1 = j*self.width
                    y1 = i*self.height + self.margin_bottom
                    self.make_content()

                    slogan_frame = Frame(x1, y1, self.width, self.height, leftPadding=self.width/5, topPadding = self.height/3, showBoundary=self.showBoundary)
                    slogan_frame.addFromList(self.slogan[counter],canvas)
                    if pos == 2:
                        pos = 1
                        counter += 1
                        if counter >= len(self.slogan):
                            counter = 0
                    else:
                        pos += 1

            return canvas

    def _set_values(self, type):

        #Define properties for paper
        if type == "apli38":
            self.nCols = 3              #Number of columns
            self.nRows = 8              #Number of rows
            self.margin_bottom = 8*mm   #Margin from bottom (in mm)
            self.margin_vertical = 5*mm   #Left column margin

            self.width = 210*mm /self.nCols  #Width for each label, 210 is the widht of an A4
            self.height = (297*mm - 2*self.margin_bottom )/self.nRows #Height of each label 297 is the height of an A4
