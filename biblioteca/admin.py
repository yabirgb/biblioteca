from django.contrib import admin

from .models import *

class CasilleroAdmin(admin.ModelAdmin):
    list_display = ("descripcion", 'numero', 'numero2', 'letra', )

class LibroAdmin(admin.ModelAdmin):
    list_display = ("titulo",'autor', 'identificador', 'etiqueta_completa',  )

admin.site.register(Clasificacion, CasilleroAdmin)
admin.site.register(Autor)
admin.site.register(Libro, LibroAdmin)
admin.site.register(Prestamo)
admin.site.register(Estado)
admin.site.register(Impresion)
# Register your models here.
