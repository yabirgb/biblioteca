$( document ).ready(function(){
    var estados = {casillero:false}
    $("#clasificacion-create-form").hide()

    $("#nuevo-casillero").click(function() {
        if (estados.casillero === false){
            $("#clasificacion-create-form").show()
            estados.casillero = true;
        }
        else
        {
            $("#clasificacion-create-form").hide()
            estados.casillero = false;
        }

    });

    //search box
    var typingTimer;                //timer identifier
    var doneTypingInterval = 300;
    $( "#search" ).on('input propertychange paste', function() {
        $(this).on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });
        //on keydown, clear the countdown
        $(this).on('keydown', function () {
            clearTimeout(typingTimer);
        });
        //user is "finished typing," do something
        function doneTyping () {
            if ($("#search").val().length !== 0){
                $.ajax({
                    type: "GET",
                    url: '/busqueda/',
                    data: { query: $("#search").val() }
                    //success: function(response){
                    //    $("#resultados").empty();
                    //    if (response.length !== 0){
                    //        response = JSON.parse(response)
                    //        html = ""
                    //        var remove = ""
                    //        for (var i = 0; i < response.length; i++){
                    //            if (response[i]["fields"]["estado"] == false){
                    //              remove = "<a class='devolver' href='/devolver/" + response[i]["fields"]["identificador"] + "'>Devolver</a>"
                    //            }
                    //            else{
                    //              remove = "<a class='prestar' href='/prestar/" + response[i]["fields"]["identificador"] + "'>Prestar</a>"
                    //            }
                    //            html += "<li>" + response[i]["fields"]["titulo"] + " de " + response[i]["fields"]["autor"] + " " + remove + "</li>"
                    //        }
                    //        $("#resultados").append(html)
                    //    }
                    //}
                });
            }
            else
            {
                $("#resultados").empty();
            }
        }
    });
});
