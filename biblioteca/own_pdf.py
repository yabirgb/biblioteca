"""
pdf_templates.py
================

Module to handle pdf templates for different printing layouts
Each template defines a

1. DocTemplate
2. PageTemplate
3. Flowables

"""

"""
    Some definitions:
        -x1 and y1 are cordenates for the frames, depend on row and column
        -"showBoundary" shows a black line in the border of the frame
        -styleN is a dictionary(you can add more elements) and apply the style
            to the elements marked with this style. More info can be obtained
            from the documentation.

"""
from reportlab.platypus import Paragraph, Frame, Image
from reportlab.lib.units import inch, mm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_CENTER

from .models import Libro
import sys
import traceback

class LabelPDF():
    def __init__(self, tag, type="apli38"):
        self.showBoundary = 0 #show a black line in the border
        self.tag = tag
        self.type = "apli38"
        self._set_values("apli38")

    def draw_frames(self, canvas ):

        if self.type=='apli38':
            counter = 1
            styles = getSampleStyleSheet()
            styleN = {"default": ParagraphStyle('default',fontName='Times-Roman',fontSize=12, alignment="TA_CENTER")}
            style2 = {"default": ParagraphStyle('default',fontName='Times-Roman',fontSize=10, alignment="TA_CENTER")}
             #List to store the non empty values
            if "copy" in self.tag:
                exclude = ["csrfmiddlewaretoken", "copy", "action"]
                filled = [x for x in self.tag if x not in exclude and self.tag[x][0] != ""]

                for i in filled:
                    pos = int(i)
                    self.tag[str(pos+1)] = self.tag[i]

                print(self.tag)

                for j in range(0,self.nCols):
                    for i in reversed(range(0, self.nRows)):
                        try:
                            x1 = j*self.width
                            y1 = i*self.height + self.margin_bottom
                            ia = self.tag[str(counter)]
                            lib = Libro.objects.get(identificador = int(ia[0]))
                            message = [Paragraph(str(lib.identificador),styleN["default"]), Paragraph(lib.etiqueta_completa ,style2["default"] )]
                            slogan_frame = Frame(x1, y1, self.width, self.height, leftPadding=self.width/5, topPadding =        self.height/3,showBoundary=self.showBoundary)
                            slogan_frame.addFromList(message,canvas)
                            lib.etiqueta = True
                            lib.save()
                        except:
                            e = sys.exc_info()[0]
                            print(e)
                            print(traceback.format_exc())

                        counter += 1

            else:
                for j in range(0,self.nCols):
                    for i in reversed(range(0, self.nRows)):
                        try:
                            x1 = j*self.width
                            y1 = i*self.height + self.margin_bottom
                            ia = self.tag[str(counter)]
                            lib = Libro.objects.get(identificador = int(ia[0]))

                            message = [Paragraph(str(lib.identificador),styleN["default"]), Paragraph( lib.clasificacion.__str__()+ "-" +lib.autor_clasificaion.upper(),styleN["default"] )]

                            slogan_frame = Frame(x1, y1, self.width, self.height, leftPadding=self.width/5, topPadding =        self.height/3,showBoundary=self.showBoundary)
                            slogan_frame.addFromList(message,canvas)
                            lib.etiqueta = True
                            lib.save()
                        except Exception as e:
                            print(e)

                        counter += 1
            return canvas

    def _set_values(self, type):

        #Define properties for paper
        if type == "apli38":
            self.nCols = 3              #Number of columns
            self.nRows = 8              #Number of rows
            self.margin_bottom = 8*mm   #Margin from bottom (in mm)
            self.margin_vertical = 5*mm   #Left column margin

            self.width = 210*mm /self.nCols  #Width for each label, 210 is the widht of an A4
            self.height = (297*mm - 2*self.margin_bottom )/self.nRows #Height of each label 297 is the height of an A4
