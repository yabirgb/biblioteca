from .models import Libro
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

class CreateBookForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_id = 'CreateBook'
        self.helper.form_method = 'post'
        self.helper.form_action = '/nuevo-libro/'

        self.helper.add_input(Submit('submit', 'Submit'))
        super(CreateBookForm, self).__init__(*args, **kwargs)
        self.fields["comentario"].widget.attrs["class"] = "materialize-textarea"

    class Meta:
        exclude = ["letras", "prestamos", "identificador", "etiqueta", "estado"]
        model = Libro

class LibroForm(ModelForm):

    class Meta:
        model = Libro
        exclude = ["estado", "prestamos", "identificador", "etiqueta"]
