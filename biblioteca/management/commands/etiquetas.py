from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from biblioteca.models import Libro
from django.db.models import Q
import json

class Command(BaseCommand):
    help = 'Create database'
    def add_arguments(self, parser):
        parser.add_argument('path', type=str)

    def _create_label_text(self, path):
            with open(path, "r") as data_file:
                data = json.load(data_file)
                a = 1
                for i in data:
                    clasificacion = i["clasif"]
                    print("Working on book number: " +str(a))
                    if clasificacion != "NO clasif":
                        l = Libro.objects.get(identificador = i["numero"])
                        l.etiqueta_completa = clasificacion
                        l.save()
                        a+=1
    def handle(self, *args, **options):
        path = options['path']
        self._create_label_text(path)
