from django.core.management.base import BaseCommand, CommandError
from biblioteca.models import Clasificacion
import json

class Command(BaseCommand):
    help = 'Create database'
    def add_arguments(self, parser):
        parser.add_argument('path', type=str)

    def _create_casilleros(self, path):
        a = 0
        with open(path) as data_file:
            data = json.load(data_file)
            for i in data:
                l, n2= "", ""
                n = i["numero"]
                des = i["descripcion"]
                if "letra" in i:
                    l = i["letra"]
                if "numero2" in i:
                    n2 = i["numero2"]

                if l and n2:
                    nuevo, created = Clasificacion.objects.get_or_create(numero = n, numero2 = n2, letra = l, descripcion = des)
                elif l and not n2:
                    nuevo, created = Clasificacion.objects.get_or_create(numero = n, letra = l, descripcion = des)
                else:
                    nuevo, created = Clasificacion.objects.get_or_create(numero = n, descripcion = des)

                if not created:
                    nuevo.save()
                a+=1
                print("Done " + str(a) + " of " + str(len(data)))

    def handle(self, *args, **options):
        path = options['path']
        print(path)
        self._create_casilleros(path)
