#first_name
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
import json

class Command(BaseCommand):
    help = 'Create database'
    def add_arguments(self, parser):
        parser.add_argument('path', type=str)

    def _create_user(self, path):
        a = 0
        with open(path) as data_file:
            data = json.load(data_file)
            for i in data:
                u = User(username = i["login"], password = i["clave"], first_name= i["nombre"])
                u.save()
                a+=1
                print("Done " + str(a) + " of " + str(len(data)))

    def handle(self, *args, **options):
        path = options['path']
        print(path)
        self._create_user(path)
