#first_name
from django.core.management.base import BaseCommand, CommandError
from biblioteca.models import Libro, Clasificacion, Prestamo
from django.contrib.auth.models import User
import json

class Command(BaseCommand):
    help = 'Populate database with books'
    def add_arguments(self, parser):
        parser.add_argument('path', type=str)

    def _create_book(self, path):
        a = 0
        with open(path, "r") as data_file:
            data = json.load(data_file)
            b = str(len(data))
            for i in data:
                autor = None
                autor_clasificaion = None
                autor_clasificaion_index = None
                estante = None
                etiqueta = False
                clasificacion = i["clasif"]
                l = clasificacion.replace("+", "-").split("-")
                estado = False
                error = False

                if i["etiq"] == "SI":
                    etiqueta = True
                if i["estado"] == "Disponible":
                    estado = True
                #estante
                try:
                    if clasificacion == "NO clasif":
                        estante = None
                        #estante None
                    elif len(l) == 2:
                        estante = Clasificacion.objects.get(numero=l[0], letra=None)
                    elif len(l) == 3 and len(l[1])  == 3:
                        estante = Clasificacion.objects.get(numero=l[0], letra=None)
                    elif len(l) == 3 and len(l[1])==1:
                        estante = Clasificacion.objects.get(numero=l[0], letra=l[1])
                    elif len(l) == 4 and len(l[1])==2 and len(l[2]) == 3:
                        estante = Clasificacion.objects.get(numero=l[0], numero2=l[1], letra=None)
                    elif len(l) == 4 and len(l[1])==2 and len(l[2]) == 1:
                        estante = Clasificacion.objects.get(numero=l[0], numero2=l[1], letra = l[2])
                    elif len(l) == 4 and len(l[1])==1:
                        estante = Clasificacion.objects.get(numero=l[0], letra=l[1])
                    elif len(l) == 5:
                        estante = Clasificacion.objects.get(numero=l[0], numero2=l[1], letra=l[2])
                    else:
                        print("error at id " + str(a) + " estante ")

                    #estante = Clasificacion.objects.get(numero = clasificacion[:2])
                except:
                    pass

                #Autor class

                try:
                    if clasificacion == "NO clasif":
                        autor_clasificaion = None
                        autor = None
                        #estante None
                    elif len(l) == 2:
                        autor_clasificaion = l[1]
                    elif len(l) == 3 and len(l[1])  == 3:
                        autor_clasificaion=str(l[1])
                    elif len(l) == 3 and len(l[1])==1:
                        autor_clasificaion = l[2]
                    elif len(l) == 4 and len(l[1])==2 and len(l[2]) == 3:
                        autor_clasificaion = l[2]
                    elif len(l) == 4 and len(l[1])==2 and len(l[2]) == 1:
                        autor_clasificaion = l[3]
                    elif len(l) == 4 and len(l[1])==1:
                        autor_clasificaion = l[2]
                    elif len(l) == 5:
                        autor_clasificaion = l[3]
                    else:
                        print("error at id " + str(a) + " autor_class")

                    #estante = Clasificacion.objects.get(numero = clasificacion[:2])
                except:
                    pass

                # Autor class index

                try:
                    if clasificacion == "NO clasif":
                        autor_clasificaion = None
                        autor = None
                        #estante None
                    elif len(l) == 3 and len(l[1])  == 3:
                        autor_clasificaion_index=l[2]
                    elif len(l) == 4 and len(l[1])==2 and len(l[2]) == 3:
                        autor_clasificaion_index = l[3]
                    elif len(l) == 4 and len(l[1])==1:
                        autor_clasificaion_index = l[3]
                    elif len(l) == 5:
                        autor_clasificaion_index = l[4]
                    elif len(l) ==2:
                        autor_clasificaion_index = None
                    else:
                        error = False

                    #estante = Clasificacion.objects.get(numero = clasificacion[:2])
                except:
                    pass

                if autor_clasificaion_index != None:
                    try:
                        autor_clasificaion_index = int(autor_clasificaion_index)
                    except:
                        autor_clasificaion_index = None




                if not error:
                    u = Libro(titulo = i["titulo"], autor = i["autor"],
                    autor_clasificaion= autor_clasificaion, autor_clasificaion_index = autor_clasificaion_index,
                    clasificacion = estante, comentario="", etiqueta = etiqueta, fecha_registro = None,
                    identificador = i["numero"], estado = estado)

                    u.save()
                    try:
                        user = User.objects.get(username=i["ultpers"])
                        p = Prestamo(usuario = user, libro_prestado = u, fecha = None)
                        p.save()
                        u.prestamos.add(p)
                        u.save()
                    except:
                        pass

                    a+=1
                print("Done " + str(a) + " of " + b)

    def handle(self, *args, **options):
        path = options['path']
        print(path)
        self._create_book(path)
