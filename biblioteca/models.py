from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Clasificacion(models.Model):
    numero = models.CharField(max_length= 3)
    numero2 = models.CharField(max_length=3, blank= True, null = True)
    letra = models.CharField(max_length= 3, blank = True, null = True)
    descripcion = models.CharField(max_length= 120)

    def __str__(self):
        if self.numero2 != None and len(self.numero2):
            return "{}-{}-{}".format(self.numero,self.numero2,self.letra)
        elif self.letra == None:
            return "{}".format(self.numero)
        else:
            return "{}-{}".format(self.numero,self.letra)

class Autor(models.Model):
    nombre = models.CharField(max_length= 240)

    def __str__(self):
        return self.nombre

class Estado(models.Model):
    estado = models.CharField(max_length=200)

    def __str__(self):
        return self.estado

class Prestamo(models.Model):
    usuario = models.ForeignKey(User)
    fecha = models.DateTimeField(auto_now_add=True, blank = True, null = True)

    def __str__(self):
        return "{}".format(self.usuario.username)

class Libro(models.Model):
    clasificacion = models.ForeignKey(Clasificacion,blank=True, null = True)
    titulo = models.CharField(max_length=300)
    autor = models.CharField(max_length= 500)
    autor_clasificaion = models.CharField(max_length=5, blank= True, null=True)
    autor_clasificaion_index = models.IntegerField(blank= True, null = True)
    estado = models.BooleanField(default= True)#Disponible
    comentario = models.TextField(blank =True, null = True)
    etiqueta = models.BooleanField(default=False)
    fecha_registro = models.DateTimeField(auto_now_add=True, blank = True, null = True)
    prestamos = models.ForeignKey(User, null =True, blank =True)
    identificador = models.IntegerField() #The index in the old database
    etiqueta_completa = models.CharField(max_length=230, blank = True, null = True)

    def __str__(self):
        return "{} de {}".format(self.titulo, self.autor)


class Impresion(models.Model):
    libros = models.ManyToManyField(Libro)
