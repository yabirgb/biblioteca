from reportlab.pdfgen.canvas import Canvas
from .own_pdf import LabelPDF
#Letter is a paper format, you can import A4 for example
from reportlab.lib.pagesizes import A4
import json
import datetime
from functools import reduce
import time
from functools import partial

from django.shortcuts import render
from django.views.generic import ListView, TemplateView
from django.views.generic.edit import UpdateView, DeleteView, CreateView, FormView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
from django.template import RequestContext
from django.shortcuts import render,redirect
from django.http import *
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.core import serializers
from django.core.urlresolvers import reverse_lazy

from django.contrib.admin.views.decorators import staff_member_required

from django.db.models import Q

from django.core.cache import cache

#Class mix to manage cache
from django.utils.cache import patch_response_headers
from django.views.decorators.cache import cache_page, never_cache

from .models import *
from .forms import LibroForm, CreateBookForm


class CacheMixin(object):
    cache_timeout = 90

    def get_cache_timeout(self):
        return self.cache_timeout

    def dispatch(self, *args, **kwargs):
        return cache_page(self.get_cache_timeout())(super(CacheMixin, self).dispatch)(*args, **kwargs)
#End cache

class HomeView(ListView):
    model = Libro
    template_name = "home.html"
    context_object_name = "libros"

    def get_queryset(self):
        return Libro.objects.order_by("-id")[:15]

    def dispatch(self, request, *args, **kwargs):
        # check if there is some video onsite
        if not request.user.is_authenticated():
            return HttpResponseRedirect(reverse("login"))
        else:
            return super(HomeView, self).dispatch(request, *args, **kwargs)

def login_user(request):
    res = {}
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse("home"))
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse("home"))
        res["error"] = "Pareja de usuario y contraseña incorrecta"
    return render(request, template_name='registration/login.html', context=res)

#listar cosas
@method_decorator(login_required, name ="dispatch")
class LibrosListView(ListView):
    model = Libro
    template_name = "listas/all.html"
    context_object_name = 'libros'

    def dispatch(self, *args, **kwargs):
        return super(LibrosListView, self).dispatch(*args, **kwargs)

class CasillerosListView(ListView):
    model = Clasificacion
    template_name ="listas/casilleros.html"
    #context_object_name = "casilleros"

    def dispatch(self, *args, **kwargs):
        return super(CasillerosListView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CasillerosListView, self).get_context_data(**kwargs)
        casilleros = Clasificacion.objects.all()
        context["lista_casilleros"] = casilleros
        return context

class LibroInCasilleroListView(ListView):
    model = Libro
    template_name = "listas/busqueda.html"
    context_object_name = "libros"
    paginate_by = 25

    def get_queryset(self, *args, **kwargs):
        return Libro.objects.all().filter(clasificacion__pk = self.kwargs["pk"])

#Buscar cosas
class Busqueda(ListView):
    model = Libro
    template_name = 'listas/busqueda.html'
    context_object_name = "libros"
    paginate_by = 50

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(Busqueda, self).get_context_data(**kwargs)
        palabra = self.request.GET.get("palabra")
        usuarios = User.objects.all()
        context["palabra"]= palabra
        context["search"] = True
        context["usuarios"] = usuarios
        return context

    def get_queryset(self):
        palabra = self.request.GET.get("palabra")
        lista = palabra.split(" ")
        queryset =  Libro.objects.filter( Q(titulo__icontains=palabra) |
                Q(identificador__icontains=palabra) | Q(autor__icontains = palabra)
                | Q(clasificacion__letra__icontains=palabra)|
                Q(clasificacion__numero__icontains=palabra)|
                Q(clasificacion__descripcion__icontains=palabra)|
                Q(etiqueta_completa__contains=palabra)|
                reduce(lambda x, y: x | y, [Q(titulo__contains=word) for word in lista])
                ).order_by('titulo')
        return queryset
#Crear cosas
@staff_member_required
def crear_casillero(request):
    if request.method == "POST":
        if len(request.POST["numero"]) and len(request.POST["descripcion"]):
            query = Clasificacion.objects.filter(descripcion = request.POST["descripcion"]).count()
            if query == 0:
                Clasificacion(numero = request.POST["numero"],
                    descripcion= request.POST["descripcion"].upper(),\
                     letra = request.POST["letra"].upper()).save()
            else:
                indice = query
                Clasificacion(numero = request.POST["numero"],\
                descripcion= request.POST["descripcion"].upper(),\
                 letra = request.POST["letra"].upper(), numero2 = indice).save()

    return HttpResponseRedirect(reverse("home"))

class LibroUpdateView(UpdateView):
    model = Libro
    form_class = LibroForm
    template_name = 'nuevo_libro.html'
    success_url = reverse_lazy('home')

    def get_object(self):
        return Libro.objects.get(identificador=self.kwargs.get("identificador"))


class LibroCreateView(CreateView):
    model = Libro
    form_class = LibroForm
    success_url = reverse_lazy("home")
    template_name = 'nuevo_libro.html'


    def form_valid(self, form):
        try:
            last = Libro.objects.all().last().identificador
        except:
            last = 0
        self.object = form.save(commit=False)
        self.object.identificador = last +1
        self.object.save()
        return HttpResponseRedirect(reverse_lazy("home"))

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(LibroCreateView, self).dispatch(*args, **kwargs)

#Devolver prestar libros
def prestar(request, identificador):
    resultados = {}
    libro = Libro.objects.get(identificador=identificador)
    usuarios = User.objects.all()
    if request.method == "POST":
        print(request.POST.get("verification"))
        if request.POST.get("verification") == "on":
            libro.prestamos = request.user
            libro.estado = False
            libro.save()
            return HttpResponseRedirect(reverse("home"))
        else:
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    resultados["libro"] = libro
    resultados["libro"] = Libro.objects.get(identificador = identificador)
    resultados["operacion"] = "coger"
    resultados["url"] = "prestar"
    resultados["usuarios"] = usuarios

    return render(request, "prestar.html", resultados)

def devolver(request, identificador):
    if request.user.is_staff:
        if request.method == "POST":
            if request.POST.get("verification") == "on":
                libro = Libro.objects.get(identificador = identificador)
                libro.estado = True
                libro.save()
                return HttpResponseRedirect(reverse_lazy("home"))
    else:
        return HttpResponseRedirect(reverse_lazy("home"))

    resultados = {}
    resultados["libro"] = Libro.objects.get(identificador = identificador)
    resultados["operacion"] = "devolver"
    resultados["url"] = "devolver"
    return render(request, "prestar.html", resultados)

#Pegatinas

def DesignLabels(request):
    if request.method == "POST":

        dic = dict(request.POST) #The request post with the books identificators
                                 #to print
        fname = "mylabels.pdf"
        #Generating response for file
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=%s' %(fname)
        #creating the canvas object. You can change "letter" with another paper type
        c  = Canvas(response, pagesize=A4)
        #LabelPDF is the class in pdf_templates that draws on the pdf
        pdf = LabelPDF(dic,type)
        #pdf.showBoundary=1  # Turns this on for debugging purposes.
        c = pdf.draw_frames(c)
        c.save()
        return response

    else:
        resultados = {}
        resultados_search = Libro.objects.all().filter(etiqueta = False)

        libros = {}
        for libro in resultados_search:
            titulo = libro.titulo
            identificador = libro.identificador

            libros[titulo] = identificador

        resultados["libros_json"] = json.dumps(libros)
        resultados["libros"] = resultados_search
    return render(request, "pegatinas.html", resultados)

class UsuariosListView(ListView):
    model = User
    context_object_name = "usuarios"
    template_name = "listas/usuarios.html"
    order_by = "name"

class MyBooksListView(ListView):
    model = Libro
    template_name = "listas/listas_libros.html"
    context_object_name= "libros"

    def get_queryset(self):
        return  Libro.objects.all().filter(prestamos__username= self.request.user,estado=False)

class PersonBooksListView(ListView):
    model = Libro
    template_name = "listas/listas_libros.html"
    context_object_name = "libros"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PersonBooksListView, self).get_context_data(**kwargs)
        context["usuario"] = self.kwargs['usuario']
        print(self.args)
        return context

    def get_queryset(self):
        return  Libro.objects.all().filter(prestamos__username= self.kwargs['usuario'],estado=False)

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(PersonBooksListView, self).dispatch(*args, **kwargs)

@staff_member_required
def prestar_administrador(request, identificador, usuario):
    resultados = {}
    libro = Libro.objects.get(identificador=identificador)
    if request.method == "POST":
        if request.POST.get("verification") == "on":
            profile = User.objects.get(username=usuario)
            libro.prestamos = profile
            libro.estado = False
            libro.save()
            return HttpResponseRedirect(reverse("home"))
        else:
            resultados["not_verified"]
    resultados["libro"] = libro
    resultados["operacion"] = "coger"
    resultados["usuario"] = usuario

    return render(request, "prestar_admin.html", resultados)
