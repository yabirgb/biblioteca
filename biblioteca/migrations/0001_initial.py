# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-01-02 11:48
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Autor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=240)),
            ],
        ),
        migrations.CreateModel(
            name='Clasificacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero', models.IntegerField()),
                ('letra', models.CharField(blank=True, max_length=3, null=True)),
                ('descripcion', models.CharField(max_length=120)),
            ],
        ),
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estado', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Libro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=300)),
                ('comentario', models.TextField()),
                ('etiqueta', models.BooleanField(default=False)),
                ('fecha_registro', models.DateTimeField(auto_now_add=True, null=True)),
                ('autor', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='biblioteca.Autor')),
                ('clasificacion', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='biblioteca.Clasificacion')),
                ('estado', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='biblioteca.Estado')),
            ],
        ),
        migrations.CreateModel(
            name='Prestamo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateTimeField(auto_now_add=True, null=True)),
                ('libro_prestado', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='biblioteca.Libro')),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='libro',
            name='prestamos',
            field=models.ManyToManyField(to='biblioteca.Prestamo'),
        ),
    ]
